import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';

import { Provider } from 'react-redux';
// Reducers
import store from './reducers';
// Routes
import AppRouters from './routers';
// Assets
import './index.css';
import '../node_modules/sweetalert2/dist/sweetalert2.min.css'

render(
  <Provider store={store}>
    <Router>
      <AppRouters />
    </Router>
  </Provider>,
  document.getElementById('root')
);
