import React, { Component } from 'react';
import { Router, Route, IndexRoute } from 'react-router';

//import logo from './logo.svg';
import '../App.css';
// material-ui
import AppBar from 'material-ui/AppBar';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';

import PropTypes from 'prop-types';

// Components
import Content from './global/Content';

injectTapEventPlugin();
// Data
// import items from '../data/menu';

class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired
  };

  render() {
    const { children } = this.props;

    return (
      <MuiThemeProvider>
        <Content body={children} />
      </MuiThemeProvider>
    );
  }
}

export default App;
