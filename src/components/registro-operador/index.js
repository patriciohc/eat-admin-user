import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux'
// material ui
import MenuItem from 'material-ui/MenuItem';
// components
import FormularioRegistro from './formularioRegistro';
import TbOperador from './tbOperador';
// actions
//import { changeUnidad, changeCategoria } from '../../actions'

class Operador extends Component  {

    render() {
        return (
            <div>
                <h3>Administracion de Operadores</h3>
                <TbOperador/>
                <FormularioRegistro roles={this.props.roles}/>
            </div>
        );
    }
}

function mapStateToProps(state) {
  return {
    roles: state.catalogos.roles
  }
}

export default connect(mapStateToProps, {  })(Operador)
