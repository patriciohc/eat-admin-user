
const initialState =  {}

const catalogos = (state = initialState, action) => {
    switch (action.type) {
        case 'CATALOGOS':
          return action.payload
        default:
            return state
    }
}

export default catalogos;
