import { createStore, applyMiddleware, combineReducers } from 'redux';

const initialState = {
    activa: null,
    lista: [],
    listaProductos: [],
    listaOperadores: [],
    imagenBase64: {}
}

const unidad = (state = initialState, action) => {
    switch (action.type) {
      case 'AGREGAR_UNIDAD':
          return {
              ...state,
              activa: {},
              lista: state.lista.concat(action.payload)
          };
        case 'LISTA_UNIDADES':
            return {
                ...state,
                activa: {},
                lista: action.payload,
            };
        //return Object.assign({}, state, {unidades: action.payload});
        case 'SET_UNIDAD':
            return {
                ...state,
                activa: action.payload,
                //activa: {id: null, direccion: "test"},
            };
        case 'CARGAR_PRODUCTOS':
          return {
            ...state,
            listaProductos: action.payload
          };
        case 'CARGAR_OPERADORES':
          return {
            ...state,
            listaOperadores: action.payload
          };
        case 'AGREGAR_PRODUCTO_UNIDAD':
          return {
            ...state,
            listaProductos: state.listaProductos.concat(action.payload),
          }
        case 'AGREGAR_OPERADOR_UNIDAD':
          return {
            ...state,
            listaOperadores: state.listaOperadores.concat(action.payload),
          }
        case 'SET_IMAGEN_UNIDAD':
          return {
            ...state,
            imagenBase64: action.payload
          };
        default:
            return state
    }
}

export default unidad;
