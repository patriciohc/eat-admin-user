import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux'
// meterial ui
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
//components
import FormularioCategoria from './formularioCategoria';
import TbCategoria from './tbCategoria';
// actions
import { changeUnidad } from '../../actions'

class Categoria extends Component {

    render() {
        return (
            <div>
                <h3>Administracion Categorias</h3>
                <TbCategoria/>
                <FormularioCategoria/>
            </div>
        );
    }
}

function mapStateToProps(state) {
  return {
      unidades: state.unidad.lista
  }
}

export default connect(mapStateToProps, { changeUnidad })(Categoria)
