import {
    getListaCategorias,
    crearCategoria,
    setCategoria,
    changeUnidad
} from './categoria';

//export {default as categoria} from './categoria';

import {
    loginCliente,
    initUsuario
} from './cliente';

import {
    getListaUnidades,
    crearUnidadad,
    setUnidad,
    initUnidad,
    setPosicion,
    editarUnidad,
    eliminarUnidad,
    setUnidadHoraAp,
    setUnidadHoraC,
    openModalProductos,
    closeModalProductos,
    setProductosAgregar,
    agregarProductosUnidad,
    setSelected
} from './unidad';

import {
    getListaProductos,
    crearProducto,
    setProducto,
    changeUnidadProducto,
    getListaProductosUnidad,
    changeCategoriaProducto
} from './producto';

import {
  getListaOperadores,
  crearOperador,
  setOperador,
} from './operador';

import {
  getListaPedidos,
  changeUnidadPedido,
  setPedido
} from './pedido'

export {
    getListaCategorias,
    crearCategoria,
    setCategoria,
    changeUnidad,
    loginCliente,
    initUsuario,
    getListaUnidades,
    crearUnidadad,
    setUnidad,
    getListaProductos,
    crearProducto,
    setProducto,
    changeUnidadProducto,
    getListaProductosUnidad,
    initUnidad,
    setPosicion,
    editarUnidad,
    eliminarUnidad,
    setUnidadHoraAp,
    setUnidadHoraC,
    openModalProductos,
    closeModalProductos,
    setProductosAgregar,
    agregarProductosUnidad,
    setSelected,
    getListaOperadores,
    crearOperador,
    setOperador,
    getListaPedidos,
    changeUnidadPedido,
    setPedido,
    changeCategoriaProducto
}
