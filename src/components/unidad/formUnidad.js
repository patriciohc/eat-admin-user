import React from "react";
import { connect } from 'react-redux';
// material-ui
import TextField from "material-ui/TextField";
import RaisedButton from 'material-ui/RaisedButton';
import TimePicker from 'material-ui/TimePicker';
import FlatButton from 'material-ui/FlatButton';
import UploadPreview from 'material-ui-upload/UploadPreview';

import { validator } from '../../helpers/validators'
// actions
import {
  crearUnidadad,
  setUnidad,
  setUnidadDireccion,
  setUnidadHoraAp,
  setUnidadHoraC,
  setImagenBase64
} from '../../actions/unidad';

const styleButton = {
  marginTop: 12,
};

class FormUnidad extends React.Component {

  errors = {}
  onChange = (event) => {
    this.props.setUnidad(event)
    validator(event, this.errors)
  }

    render() {
      return (
        <div>
      <form>
      <TextField
        name="nombre"
        floatingLabelText="Nombre*"
        floatingLabelFixed={true}
        fullWidth={true}
        value={this.props.unidad.nombre}
        onChange={this.onChange}
        required
        errorText={this.errors['nombre']}
      />
        <TextField
          name="referencia"
          floatingLabelText="Referencia"
          fullWidth={true}
          floatingLabelFixed={true}
          value={this.props.unidad.referencia}
          onChange={this.onChange}
          required
          errorText={this.errors['referencia']}
        />
        <TextField
          name="direccion"
          floatingLabelText="Direcciòn*"
          fullWidth={true}
          floatingLabelFixed={true}
          disabled={true}
          value={this.props.unidad.direccion}
        />
        <TextField
          name="palabras_clave"
          floatingLabelText="Resumen"
          multiLine={true}
          rows={3}
          fullWidth={true}
          floatingLabelFixed={true}
          value={this.props.unidad.palabras_clave}
          onChange={this.onChange}
          errorText={this.errors['palabras_clave']}
        />
        <TextField
          name="telefono"
          floatingLabelText="Telefono"
          fullWidth={true}
          floatingLabelFixed={true}
          value={this.props.unidad.telefono}
          onChange={this.onChange}
          errorText={this.errors['telefono']}
        />
        <TimePicker
            floatingLabelFixed={true}
            fullWidth={true}
            name="hora_apetura"
            hintText="Hora de Apertura*"
            value={this.props.unidad.horaApetura}
            onChange={this.props.setUnidadHoraAp}
        />
        <TimePicker
            floatingLabelFixed={true}
            fullWidth={true}
            name="hora_cierre"
            hintText="Hora de cierre*"
            value={this.props.unidad.horaCierre}
            onChange={this.props.setUnidadHoraC}
        />
        <UploadPreview
          title="Imagen"
          label="Seleccionar imagen"
          initialItems={this.props.unidad.imagenBase64}
          onChange={this.props.setImagenBase64}/>
      </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    unidad: state.unidad.activa
  }
}

export default connect(mapStateToProps, {
  crearUnidadad,
  setUnidad,
  setUnidadHoraAp,
  setUnidadHoraC,
  setImagenBase64
})(FormUnidad)
