import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux'
// material ui
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
// components
import FormularioProducto from './formularioProducto';
import TbProducto from './tbProducto';
// actions
import { changeCategoria, getListaProductos, initProducto } from '../../actions/producto'
import { getListaCategorias } from '../../actions/categoria'

const styleButton = {
  margin: 10,
};

class Producto extends Component  {

  componentWillMount() {
    this.props.getListaCategorias();
  }

    renderSelectCategoria() {
        return this.props.categorias.map( (item, index) => {
            return (
                <MenuItem value={item.id} primaryText={item.nombre} />
            )
        });
    }

    render() {
        return (
            <div>
                <div className="col-md-7">
                    <h3>Administracion Productos</h3>
                </div>
                <div className="col-md-5" style={{textAlign: "right"}}>
                    <RaisedButton
                        style={styleButton}
                        label="Registrar nuevo producto"
                        onClick={() => this.props.initProducto()} primary
                    />
                </div>
                <SelectField
                    floatingLabelText="Seleccione una Categoria"
                    value={this.props.categoriaSeleccionada}
                    onChange={this.changeUnidad}
                    >
                        {this.renderSelectCategoria()}
                </SelectField>
                <TbProducto categorias={this.props.categorias}/>
                { this.props.producto && typeof(this.props.producto.id) != "undefined" &&
                  <FormularioProducto
                    categorias={this.props.categorias}
                    producto={this.props.producto}/>
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
  return {
    unidades: state.unidad.lista,
    categorias: state.categoria.lista,
    unidadSeleccionada: state.categoria.unidadSeleccionada,
    producto: state.producto.activa
  }
}

export default connect(mapStateToProps, {
  getListaCategorias,
  initProducto
})(Producto)
