
export function validator(event, errors) {
  var input = event.target
  delete errors[input.name]
  if (input.required && !input.value)
    errors[input.name] = "Este campo es requerido"
}

export function validatorForm (requires, json) {
  for(let i = 0; i < requires.length; i++) {
    if (!json[requires[i]]) return false;
  }
  return true;
}
