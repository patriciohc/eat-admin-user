import axios from 'axios'
import { SERVER } from '../settings';

// pedidos
export function getListaPedidos() {
  console.log("execute --> getListaPedidos");
  return (dispatch, getState) => {
    var id_unidad = getState().pedido.unidadSeleccionada;
    axios.get(SERVER + '/api/pedido?id_unidad='+id_unidad)
    .then((response) => {
      console.log(response.data);
      dispatch( { type: 'LISTA_PEDIDOS', payload: response.data } )
    })
    .catch( err => {
      console.log(err);
    });
  }
}

// repartidores
export function getListaRepartidores() {
  console.log("execute --> getListaRepartidores");
  return (dispatch, getState) => {
    var id_unidad = getState().pedido.unidadSeleccionada;
    axios.get(SERVER + '/api/unidad-operador?id_unidad='+id_unidad)
    .then((response) => {
      console.log(response.data);
      dispatch( { type: 'LISTA_REPARTIDORES_PEDIDO', payload: response.data } )
    })
    .catch( err => {
      console.log(err);
    });
  }
}

export function setPedido (index) {
  console.log("execute --> setPedido");
  return (dispatch, getState) => {
    var pedido = getState().pedido.lista[index];
    dispatch( { type: 'SET_PEDIDO', payload: pedido } )
  }
}

export function cambiarEstatus (estatus) {
  console.log("execute --> cambiarEstatus");
  return (dispatch, getState) => {
    var pedido = Object.assign({}, getState().pedido.activa)
    axios.put(SERVER + '/api/pedido-estatus?id='+pedido.id, {estatus})
    .then((response) => {
      pedido.estatus = estatus
      console.log(response.data);
      dispatch( { type: 'SET_PEDIDO', payload: pedido } )
    })
    .catch( err => {
      console.log(err);
    });
  }
}

export function asignarRepartidor (id_repartidor) {
  console.log("execute --> cambiarEstatus");
  return (dispatch, getState) => {
    var pedido = Object.assign({}, getState().pedido.activa)
    axios.put(SERVER + '/api/pedido-repartidor?id='+pedido.id, {id_repartidor})
    .then((response) => {
      pedido.estatus = 3
      console.log(response.data);
      dispatch( { type: 'SET_PEDIDO', payload: pedido } )
    })
    .catch( err => {
      console.log(err);
    });
  }
}

export function changeUnidadPedido(event, index, val) {
  console.log("execute --> changeUnidadPedido");
  return (dispatch, getState) => {
    dispatch( { type: 'SET_UNIDAD_SELECCIONADA', payload: val} )
  }
}
