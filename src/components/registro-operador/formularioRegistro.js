import React from "react";
import TextField from "material-ui/TextField";
import RaisedButton from 'material-ui/RaisedButton';
import UploadPreview from 'material-ui-upload/UploadPreview';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import { connect } from 'react-redux';

import { validator } from '../../helpers/validators'
// actions
import { crearOperador, setOperador, setRolOperador } from '../../actions/operador';

class FormularioRegistro extends React.Component {
  errors = {}
  onChange = (event) => {
    this.props.setOperador(event)
    validator(event, this.errors)
  }

  renderListaRoles() {
    let items = []
    for (let key in this.props.roles) {
      items.push((<MenuItem value={key} primaryText={this.props.roles[key]} />))
    }
    return items
  }

  render() {
    return (
      <form className="row">
        <div className="col-md-6">
        <TextField
          name="nombre"
          floatingLabelText="Nombre*"
          floatingLabelFixed={true}
          fullWidth={true}
          value={this.props.operador.nombre}
          onChange={this.onChange}
          required
          errorText={this.errors['nombre']}
        />
        <TextField
          name="apellido_paterno"
          floatingLabelText="Apellido Paterno*"
          fullWidth={true}
          floatingLabelFixed={true}
          value={this.props.operador.apellido_paterno}
          onChange={this.onChange}
          required
          errorText={this.errors['apellido_paterno']}
        />
        <TextField
          name="apellido_materno"
          floatingLabelText="Apellido Materno*"
          fullWidth={true}
          floatingLabelFixed={true}
          value={this.props.operador.apellido_materno}
          onChange={this.onChange}
          required
          errorText={this.errors['apellido_materno']}
        />
        <TextField
          name="correo_electronico"
          floatingLabelText="Email*"
          fullWidth={true}
          floatingLabelFixed={true}
          value={this.props.operador.correo_electronico}
          onChange={this.onChange}
          required
          errorText={this.errors['correo_electronico']}
        />
        <TextField
          name="password"
          floatingLabelText="Contraseña*"
          type="password"
          fullWidth={true}
          floatingLabelFixed={true}
          value={this.props.operador.password}
          onChange={this.onChange}
          required
          errorText={this.errors['password']}
        />
        <SelectField
          name="rol"
          floatingLabelText="Seleccione el Rol"
          value={this.props.operador.rol}
          required
          onChange={this.props.setRolOperador}>
          {this.renderListaRoles()}
        </SelectField>

        </div>

        <div className="col-md-6" style={{paddingTop:40}}>
          <UploadPreview
            title="Foto"
            label="Seleccionar imagen"
            initialItems={this.props.operador.imagenBase64}
            onChange={this.props.setImagenBase64}/>
          <div className="col-md-12" style={{paddingTop:30}}>
            { this.props.operador.id == null ?
              (<RaisedButton className="pull-right" label="Registrar Operador" onClick={this.props.crearOperador} primary />) :
              (<RaisedButton className="pull-right" label="Guardar Edición" onClick={this.props.GuardarEdicionProducto} primary />)
            }
          </div>
        </div>
      </form>
    );
  }
}

function mapStateToProps(state) {
  return {
    operador: state.operador.activa
  }
}

export default connect(mapStateToProps, {
  crearOperador,
  setOperador,
  setRolOperador
})(FormularioRegistro)
