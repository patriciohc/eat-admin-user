import axios from 'axios'
import { guardar as guardarImagen, getExtencion } from './imagen';
import swal from 'sweetalert2'
import { validatorForm } from '../helpers/validators'
import { SERVER } from '../settings';

export function initProducto() {
  console.log("execute --> initProducto")
  return (dispatch, getState) => {
    var obj = {
      id: null
    };
    dispatch( { type: 'SET_PRODUCTO', payload: obj } )
  }
}

export function setImagenBase64(picture) {
  return (dispatch, getState) => {
    dispatch({type: 'SET_IMAGEN_PRODUCTO', payload: picture})
  }
}

export function editarProducto (index) {
  console.log("execute --> editarProducto");
  return (dispatch, getState) => {
    var producto = getState().producto.lista[index]
    dispatch( { type: 'SET_PRODUCTO', payload: producto } )
  }
}

export function GuardarEdicionProducto () {

}

// lista de productos
export function getListaProductos() {
  console.log("execute --> getListaCategorias");
  return (dispatch, getState) => {
    let id_cliente = getState().cliente.id
    axios.get(SERVER + '/api/producto?id_cliente='+id_cliente)
    .then((response) => {
      console.log(response.data);
      dispatch( { type: 'LISTA_PRODUCTOS', payload: response.data } )
    })
    .catch(err => {
      console.log(err);
    });
  }
}

const requires = [
  'nombre',
  'descripcion',
  'precio_publico',
  'id_categoria'
]
export function crearProducto() {
  console.log("execute --> crearUnidadad");
  return (dispatch, getState) => {
    var state = getState();
    var producto = Object.assign({}, state.producto.activa)
    if (!validatorForm(requires, producto)) {
      swal("Los campos marcados con * son requeridos")
      return
    }
    producto.id_cliente = state.cliente.id
    var imagenBase64;
    for (let value in state.producto.imagenBase64) {
      imagenBase64 = state.producto.imagenBase64[value];
      producto.imagen = producto.id_cliente + "-prod-" + (new Date()).getTime() + "." + getExtencion(imagenBase64)
      break
    }
    delete producto.id;
    axios.post(SERVER + '/api/producto', producto)
    .then((response) => {
      producto.id = response.data.id;
      return guardarImagen(imagenBase64, producto.imagen)
    })
    .then(result => {
      console.log(result);
      producto.imagen = result
      dispatch( { type: 'CREAR_PRODUCTO', payload: producto } )
    })
    .catch(err => {
      console.log(err);
    })
  }
}

export function setProducto(event) {
  console.log("execute --> setCategoria");
  const target = event.target;
  const value = target.type === 'checkbox' ? target.checked : target.value;
  const name = target.name;
  return (dispatch, getState) => {
    var producto = Object.assign({}, getState().producto.activa);
    producto[name] = value;
    dispatch( { type: 'SET_PRODUCTO', payload: producto } )
  }
}

export function changeFiltros(event, index, value) {
  var name = event.tarjet;
  return (dispatch, getState) => {
    let state = getState();
    let unidad = state.producto.unidadSeleccionada;
    let categoria = state.producto.categoriaSeleccionada;
    axios.get(SERVER + '/api/producto?id_unidad='+value)
    .then((response) => {
      console.log(response.data);
      dispatch({
        type: 'CHANGE_FILTROS',
        payload: {
          lista: response.data,
          unidadSeleccionada: value
        }
      })
    })
  }
}

export function changeCategoriaProducto(event, index, value) {
  return (dispatch, getState) => {
    var producto = Object.assign({}, getState().producto.activa);
    producto.id_categoria = value;
    dispatch({ type: 'SET_PRODUCTO', payload: producto })
  }
}

export function eliminarProducto(index) {
  console.log("execute --> eliminarProducto");
  return (dispatch, getState) => {
    let objs = getState().producto.lista;
    let id = objs[index].id;
    axios.delete(SERVER + '/api/producto?id='+id)
    .then((response) => {
      let id_cliente = getState().cliente.id
      axios.get(SERVER + '/api/producto?id_cliente='+id_cliente)
      .then( response => {
        console.log(response.data);
        dispatch( { type: 'LISTA_PRODUCTOS', payload: response.data } )
      })
      .catch( err => {
        console.log(err);
      });
    })
    .catch( err => {
      console.log(err);
    });
  }
}
