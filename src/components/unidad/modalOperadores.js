import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux'
import {
  agregarOperadoresUnidad,
  setSelectedOperadores
} from '../../actions/unidad'

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Checkbox from 'material-ui/Checkbox';

const styles = {
  block: {
    maxWidth: 250,
  },
  checkbox: {
    marginBottom: 16,
  },
};

class ModalOperadores extends Component  {

  state = {
    checked: false,
  }

  updateCheck() {
    this.setState((oldState) => {
      return {
        checked: !oldState.checked,
      };
    });
  }

  renderList() {
    return this.props.operador.map((obj, index) => {
      return (
        <TableRow key={obj.id} >
        <TableRowColumn>{obj.id}</TableRowColumn>
        <TableRowColumn>{obj.nombre}</TableRowColumn>
        <TableRowColumn>{obj.apellido_paterno}</TableRowColumn>
        <TableRowColumn>{obj.apellido_materno}</TableRowColumn>
        <TableRowColumn>{obj.correo_electronico}</TableRowColumn>
        <TableRowColumn>{obj.rol}</TableRowColumn>
          <TableRowColumn>
            <Checkbox
              checked={obj.selected}
              onCheck={() => this.props.setSelectedOperadores(index)}
              style={styles.checkbox}
            />
          </TableRowColumn>
        </TableRow>
      )
    })
  }

  render() {
    const actions = [
    <FlatButton
      label="Cerrar"
      primary={true}
      onClick={this.props.onClose}
    />,
  ];
    return (
      <Dialog
        title="Seleccione los productos que desea agregar"
        modal={false}
        actions={actions}
        open={this.props.open}
        onRequestClose={this.props.onClose}
        autoScrollBodyContent={true}>
        <FlatButton
          label="Agregar"
          primary={true}
          onClick={this.props.agregarOperadoresUnidad}
        />
        <Table

          selectable={false}
          multiSelectable={false}>
          <TableHeader
            displaySelectAll={false}
            adjustForCheckbox={false}
            enableSelectAll={false}>
            <TableRow>
              <TableHeaderColumn>Id</TableHeaderColumn>
              <TableHeaderColumn>Nombre</TableHeaderColumn>
              <TableHeaderColumn>Ap Paterno</TableHeaderColumn>
              <TableHeaderColumn>Ap Materno</TableHeaderColumn>
              <TableHeaderColumn>Email</TableHeaderColumn>
              <TableHeaderColumn>Rol</TableHeaderColumn>
              <TableHeaderColumn></TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody
            displayRowCheckbox={false}>
            {this.renderList()}
          </TableBody>
        </Table>
      </Dialog>
    );
  }
}

function mapStateToProps(state) {
  return {
    operador: state.operador.lista,
  }
}

export default connect(mapStateToProps, {
  agregarOperadoresUnidad,
  setSelectedOperadores
})(ModalOperadores)
