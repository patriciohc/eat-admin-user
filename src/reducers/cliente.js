
const initialState = {
      id: null,
      razon_social: "",
      correo_electronico: "",
  }

const cliente = (state = initialState, action) => {
        switch (action.type) {
            case 'LOGIN':
                localStorage.setItem("usuario", JSON.stringify(action.payload));
                return action.payload;
            case 'INIT_USUARIO':
                return action.payload;
            default:
                return state;
        }
}


export default cliente;
