import axios from 'axios'
import { guardar as guardarImagen, getExtencion } from './imagen';
import swal from 'sweetalert2';
import { validatorForm } from '../helpers/validators';
import { SERVER } from '../settings';

// categorias
export function getListaOperadores() {
  console.log("execute --> getListaOperadores");
    return (dispatch, getState) => {
      var idCliente = getState().cliente.id;
        axios.get(SERVER + '/api/operador?id_cliente='+idCliente)
            .then((response) => {
                console.log(response.data);
                dispatch( { type: 'LISTA_OPERADOR', payload: response.data } )
            })
            .catch( err => {
              console.log(err);
            });
    }
}

const requires = ['nombre']
export function crearOperador() {
  console.log("execute --> crearOperador");
  //console.log(categoria);
  return (dispatch, getState) => {
    var state = getState();
    var operador = Object.assign({}, state.operador.activa);
    if (!validatorForm(requires, operador)) {
      swal("Los campos marcados con * son requeridos")
      return
    }
    operador.id_cliente = state.cliente.id;
    var imagenBase64;
    for (let value in state.operador.imagenBase64) {
      imagenBase64 = state.operador.imagenBase64[value];
      operador.imagen = operador.id_cliente + "-ope-" + (new Date()).getTime() + "." + getExtencion(imagenBase64)
      break
    }
    delete operador.id
    axios.post(SERVER + '/api/operador', operador)
    .then((response) => {
      operador.id = response.data.id;
      return guardarImagen(imagenBase64, operador.imagen)
    })
    .then(result => {
      dispatch( { type: 'CREAR_OPERADOR', payload: operador } )
    })
    .catch(err => {
      console.log(err);
    });
  }
}

export function setOperador(event) {
    console.log("execute --> setOperador");
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    return (dispatch, getState) => {
      var operador = Object.assign({}, getState().operador.activa);
      operador[name] = value;
      console.log(operador);
      dispatch( { type: 'SET_OPERADOR', payload: operador } )
    }
}

export function setRolOperador(event, index, value) {
    console.log("execute --> setRolOperador");
    return (dispatch, getState) => {
      var operador = Object.assign({}, getState().operador.activa);
      operador.rol = value;
      dispatch( { type: 'SET_OPERADOR', payload: operador } )
    }
}
