import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux'
import { getListaOperadores } from '../../actions'

import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';

class TbOperador extends Component  {

  componentWillMount() {
    this.props.getListaOperadores();
  }

    renderOperadorList() {
        return this.props.operadores.map((operador) => {
            return (
                <TableRow key={operador.id}>
                    <TableRowColumn>{operador.id}</TableRowColumn>
                    <TableRowColumn>{operador.nombre}</TableRowColumn>
                    <TableRowColumn>{operador.apellido_paterno}</TableRowColumn>
                    <TableRowColumn>{operador.apellido_materno}</TableRowColumn>
                    <TableRowColumn>{operador.correo_electronico}</TableRowColumn>
                </TableRow>
            )
        })
    }

    render() {
        return (
            <Table>
            <TableHeader
                displaySelectAll={false}
                adjustForCheckbox={false}
            >
                <TableRow>
                    <TableHeaderColumn>Id</TableHeaderColumn>
                    <TableHeaderColumn>Nombre</TableHeaderColumn>
                    <TableHeaderColumn>Apellodo Paterno</TableHeaderColumn>
                    <TableHeaderColumn>Apellido Materno</TableHeaderColumn>
                    <TableHeaderColumn>Email</TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {this.renderOperadorList()}
            </TableBody>
            </Table>
        );
    }
}

function mapStateToProps(state) {
  return {
    operadores: state.operador.lista
  }
}

export default connect(mapStateToProps, { getListaOperadores })(TbOperador)
