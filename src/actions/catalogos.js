import axios from 'axios';
import { SERVER } from '../settings';

// categorias
export function getCatalogos() {
  console.log("execute --> getCatalogos");
    return (dispatch, getState) => {
      axios.get(SERVER + '/api/catalogos/')
        .then((response) => {
            console.log(response.data);
            dispatch( { type: 'CATALOGOS', payload: response.data } )
        })
        .catch( err => {
          console.log(err);
        });
    }
}
