import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux'
import {
  eliminarProducto,
  getListaProductos,
  editarProducto
} from '../../actions/producto'

import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';

class TbProducto extends Component  {

  componentWillMount() {
    this.props.getListaProductos();
  }

    renderProductoList() {
        return this.props.productos.map((producto, index) => {
            var categoria = this.props.categorias.find(item => item.id == producto.id_categoria);
            return (
                <TableRow key={producto.id}>
                    <TableRowColumn>{producto.id}</TableRowColumn>
                    <TableRowColumn>{producto.nombre}</TableRowColumn>
                    <TableRowColumn>{producto.descripcion}</TableRowColumn>
                    <TableRowColumn>{producto.precio_publico}</TableRowColumn>
                    <TableRowColumn>{categoria && categoria.nombre}</TableRowColumn>
                    <TableRowColumn>
                        <FlatButton
                            label="Editar"
                            primary
                            onClick={() => this.props.editarProducto(index)}
                        />
                    </TableRowColumn>
                    <TableRowColumn>
                        <FlatButton
                            label="Eliminar"
                            secondary
                            onClick={() => this.props.eliminarProducto(index)}
                        />
                    </TableRowColumn>
                </TableRow>
            )
        })
    }

    render() {
        return (
            <Table>
            <TableHeader
                displaySelectAll={false}
                adjustForCheckbox={false}>
                <TableRow>
                    <TableHeaderColumn>Id</TableHeaderColumn>
                    <TableHeaderColumn>Nombre</TableHeaderColumn>
                    <TableHeaderColumn>Descripcion</TableHeaderColumn>
                    <TableHeaderColumn>Precio</TableHeaderColumn>
                    <TableHeaderColumn>Categoria</TableHeaderColumn>
                    <TableHeaderColumn></TableHeaderColumn>
                    <TableHeaderColumn></TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {this.renderProductoList()}
            </TableBody>
            </Table>
        );
    }
}

function mapStateToProps(state) {
  return {
    productos: state.producto.lista
  }
}

export default connect(mapStateToProps, {
  getListaProductos,
  eliminarProducto,
  editarProducto
})(TbProducto)
