
const initialState =  {
  activa: {},
  lista: [],
  imagenBase64: {}
}

const categoria = (state = initialState, action) => {
  switch (action.type) {
    case 'LISTA_CATEGORIAS':
    return {
      activa: {
        nombre: "",
        descripcion: "",
        id_unidad: "",
        imagen: "",
      },
      lista: action.payload
    };
    case 'CREAR_CATEGORIA':
    return {
      activa: {
        nombre: "",
        descripcion: "",
        id_unidad: "",
        imagen: "",
      },
      imagenBase64: {},
      lista: state.lista.concat(action.payload)
    };
    //return Object.assign({}, state, {unidades: action.payload});
    case 'SET_CATEGORIA':
    return {
      activa: action.payload,
      lista: state.lista
    };
    case 'CHANGE_UNIDAD':
    return {
      activa: state.activa,
      lista: action.payload.lista,
      unidadSeleccionada: action.payload.unidadSeleccionada
    }
    case 'SET_IMAGEN_CATEGORIA':
    return {
      ...state,
      imagenBase64: action.payload
    };
    default:
    return state
  }
}

export default categoria;
