import React from "react";
import TextField from "material-ui/TextField";
import RaisedButton from 'material-ui/RaisedButton';
import Upload from 'material-ui-upload/Upload';
import UploadPreview from 'material-ui-upload/UploadPreview';

import { validator } from '../../helpers/validators'

import { connect } from 'react-redux';

// actions
import {
  crearCategoria,
  setCategoria,
  setImagenBase64
} from '../../actions/categoria';

class FormularioCategoria extends React.Component {

  errors = {}
  onChange = (event) => {
    this.props.setCategoria(event)
    validator(event, this.errors)
  }

    render() {return (
      <form>
        <div className="col-md-6">
        <TextField
          name="nombre"
          floatingLabelText="Nombre*"
          floatingLabelFixed={true}
          fullWidth={true}
          value={this.props.categoria.nombre}
          onChange={this.onChange}
          required
          errorText={this.errors['nombre']}
        />
        <TextField
          name="descripcion"
          floatingLabelText="Descripcion*"
          fullWidth={true}
          floatingLabelFixed={true}
          value={this.props.categoria.descripcion}
          onChange={this.onChange}
          required
          errorText={this.errors['descripcion']}
        />
        </div>
        <div className="col-md-6" style={{paddingTop:40, paddingBottom: 40}}>
          <UploadPreview
            title="Imagen"
            label="Seleccionar imagen"
            initialItems={this.props.categoria.imagenBase64}
            onChange={this.props.setImagenBase64}/>
          <div className="col-md-12" style={{paddingTop:30}}>
            { this.props.categoria.id == null ?
              (<RaisedButton className="pull-right" label="Registrar Categoria" onClick={this.props.crearCategoria} primary />) :
              (<RaisedButton className="pull-right" label="Guardar Edición" onClick={this.props.GuardarEdicionProducto} primary />)
            }
          </div>
        </div>
      </form>
  )};
}

function mapStateToProps(state) {
  return {
    categoria: state.categoria.activa
  }
}

export default connect(mapStateToProps, {
  crearCategoria,
  setCategoria,
  setImagenBase64
})(FormularioCategoria)
