import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux'
import { agregarProductosUnidad, setSelected } from '../../actions/unidad'

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Checkbox from 'material-ui/Checkbox';

const styles = {
  block: {
    maxWidth: 250,
  },
  checkbox: {
    marginBottom: 16,
  },
};

class ModalProductos extends Component  {

  state = {
    checked: false,
  }

  updateCheck() {
    this.setState((oldState) => {
      return {
        checked: !oldState.checked,
      };
    });
  }

  renderProductoList() {
    return this.props.productos.map((producto, index) => {
      return (
        <TableRow key={producto.id} >
        <TableRowColumn>{producto.id}</TableRowColumn>
        <TableRowColumn>{producto.nombre}</TableRowColumn>
        <TableRowColumn>{producto.precio}</TableRowColumn>
        <TableRowColumn>{producto.id_categoria}</TableRowColumn>
        <TableRowColumn>
        <Checkbox
          checked={producto.selected}
          onCheck={() => this.props.setSelected(index)}
          style={styles.checkbox}
        />
        </TableRowColumn>

        </TableRow>
      )
    })
  }

  render() {
    const actions = [
    <FlatButton
      label="Cerrar"
      primary={true}
      onClick={this.props.onClose}
    />,
  ];
    return (
      <Dialog
        title="Seleccione los productos que desea agregar"
        modal={false}
        actions={actions}
        open={this.props.open}
        onRequestClose={this.props.onClose}
        autoScrollBodyContent={true}>
        <FlatButton
          label="Agregar"
          primary={true}
          onClick={this.props.agregarProductosUnidad}
        />
        <Table

          selectable={false}
          multiSelectable={false}>
          <TableHeader
            displaySelectAll={false}
            adjustForCheckbox={false}
            enableSelectAll={false}>
            <TableRow>
              <TableHeaderColumn>Id</TableHeaderColumn>
              <TableHeaderColumn>Nombre</TableHeaderColumn>
              <TableHeaderColumn>Precio</TableHeaderColumn>
              <TableHeaderColumn>categoria</TableHeaderColumn>
              <TableHeaderColumn></TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody
            displayRowCheckbox={false}>
            {this.renderProductoList()}
          </TableBody>
        </Table>
      </Dialog>
    );
  }
}

function mapStateToProps(state) {
  return {
    productos: state.producto.lista,
  }
}

export default connect(mapStateToProps, {
  agregarProductosUnidad,
  setSelected
})(ModalProductos)
