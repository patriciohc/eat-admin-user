
const initialState =  {
    activa: {},
    lista: [],
    unidadSeleccionada: 1,
    categoriaSeleccionada: 1,
    imagenBase64: {}
}

const producto = (state = initialState, action) => {
    switch (action.type) {
        case 'LISTA_PRODUCTOS':
        return {
            ...state,
            activa: {
                nombre: "",
                descripcion: "",
                id_unidad: "",
                imagen: "",
            },
            lista: action.payload,
        };
        case 'CREAR_PRODUCTO':
        return {
            ...state,
            activa: {
                nombre: "",
                descripcion: "",
                id_unidad: "",
                imagen: "",
            },
            imagenBase64: {},
            lista: state.lista.concat(action.payload),
        };
        //return Object.assign({}, state, {unidades: action.payload});
        case 'SET_PRODUCTO':
            return {
                ...state,
                activa: action.payload
            };
        case 'CHANGE_FILTROS':
            return {
                ...state,
                lista: action.payload.lista,
                unidadSeleccionada: action.payload.unidadSeleccionada,
                categoriaSeleccionada: action.payload.categoriaSeleccionada,
            };
        case 'SET_PRODUCTOS_SELECTED':
          return {
            ...state,
            lista: action.payload
          }
        case 'SET_IMAGEN_PRODUCTO':
          return {
            ...state,
            imagenBase64: action.payload
          };
        default:
            return state
    }
}

export default producto;
