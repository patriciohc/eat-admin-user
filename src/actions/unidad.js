import axios from 'axios';
import { guardar as guardarImagen, getExtencion } from './imagen';
import swal from 'sweetalert2'
import { validatorForm } from '../helpers/validators'
import { SERVER } from '../settings';

// {
//   headers: {'X-Custom-Header': 'foobar'}
// }

// unidades
export function getListaUnidades() {
  console.log(" --> getListaUnidades");
    return (dispatch, getState) => {
        axios.get(SERVER + '/api/unidad-cliente/')
            .then( response => {
                console.log(response.data);
                dispatch( { type: 'LISTA_UNIDADES', payload: response.data } )
            })
            .catch( err => {
                console.log(err);
            });
    }
}

export function initUnidad() {
    console.log("execute --> initUnidad")
    return (dispatch, getState) => {
        var unidad = {
          id: null
        };
        dispatch( { type: 'SET_UNIDAD', payload: unidad } )
    }
}

const requires = [
  'nombre',
  'direccion',
  'hora_apetura',
  'hora_cierre'
]
export function crearUnidadad() {
  console.log("execute --> crearUnidadad");
    return (dispatch, getState) => {
        var state = getState();
        var unidad = state.unidad.activa;
        if (!validatorForm(requires, unidad)) {
          swal("Los campos marcados con * son requeridos")
          return
        }
        unidad.id_cliente = state.cliente.id;
        var imagenBase64;
        for (let value in state.unidad.imagenBase64) {
          imagenBase64 = state.unidad.imagenBase64[value];
          unidad.imagen = unidad.id_cliente + "-uni-" + (new Date()).getTime() + "." + getExtencion(imagenBase64)
          break
        }
        delete unidad.id
        axios.post(SERVER + '/api/unidad', unidad)
            .then((response) => {
                unidad.id = response.data.id;
                return guardarImagen(imagenBase64, unidad.imagen)
            })
            .then(result => {
              console.log(result);
              dispatch( { type: 'AGREGAR_UNIDAD', payload: unidad } )
            })
            .catch(err => {
              console.log(err);
            });
    }
}

export function setUnidad(event) {
    console.log("execute --> setUnidadad");
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    return (dispatch, getState) => {
      var unidad = Object.assign({}, getState().unidad.activa);
      unidad[name] = value;
      console.log(unidad);
      dispatch( { type: 'SET_UNIDAD', payload: unidad } )
    }
}

export function setPosicion(place) {
  var lat = place.geometry.location.lat();
  var lng = place.geometry.location.lng();
  var posicion = {lat, lng};

    return (dispatch, getState) => {
        var unidad = Object.assign({}, getState().unidad.activa);
        unidad.lat = lat;
        unidad.lng = lng;
        axios.get(`${SERVER}/maps/api/geocode/json?latlng=${lat},${lng}&key=AIzaSyBSMWIMf9gOOAjES6xMFp3vzCu5WH7naZI`)
        .then( response => {
          unidad.direccion = response.data.results[0].formatted_address
          dispatch({ type: 'SET_UNIDAD', payload: unidad});
        })
        dispatch({ type: 'SET_POSICION', payload: posicion});
    }
}

export function setUnidadHoraAp(even, object) {
  var hora = object.getHours();
  var minutos = object.getMinutes();
  return (dispatch, getState) => {
      var unidad = Object.assign({}, getState().unidad.activa);
      unidad.hora_apetura = hora + ":" + minutos + ":00";
      unidad.horaApetura = object;
      dispatch({ type: 'SET_UNIDAD', payload: unidad});
  }
}

export function setUnidadHoraC(even, object) {
  var hora = object.getHours();
  var minutos = object.getMinutes();
  return (dispatch, getState) => {
      var unidad = Object.assign({}, getState().unidad.activa);
      unidad.hora_cierre = hora + ":" + minutos + ":00";
      unidad.horaCierre = object; //hora + ":" + minutos + ":00";
      dispatch({ type: 'SET_UNIDAD', payload: unidad});
  }
}

export function editarUnidad(index) {
  return (dispatch, getState) => {
    let unidades = getState().unidad.lista;
    let unidad = unidades[index];
    dispatch( { type: 'SET_UNIDAD', payload: unidad } )
  }
}

export function cargarProductos(index) {
  return (dispatch, getState) => {
    let unidades = getState().unidad.lista;
    let unidad = unidades[index];
    console.log(unidad);
    axios.get(SERVER + '/api/unidad-producto?id_unidad='+unidad.id)
        .then((response) => {
          console.log(response);
          dispatch( { type: 'CARGAR_PRODUCTOS', payload: response.data } )
        })
  }
}

export function cargarOperadores(index) {
  return (dispatch, getState) => {
    let unidades = getState().unidad.lista;
    let unidad = unidades[index];
    console.log(unidad);
    axios.get(SERVER + '/api/unidad-operador?id_unidad='+unidad.id)
        .then((response) => {
          console.log(response);
          dispatch( { type: 'CARGAR_OPERADORES', payload: response.data } )
        })
  }
}

export function eliminarUnidad(index) {
  console.log("execute --> eliminarUnidad");
    return (dispatch, getState) => {
      let unidades = getState().unidad.lista;
      let idUnidad = unidades[index].id;
        axios.delete(SERVER + '/api/unidad?id='+idUnidad)
            .then((response) => {
              axios.get(SERVER + '/api/unidad')
                  .then( response => {
                      console.log(response.data);
                      dispatch( { type: 'LISTA_UNIDADES', payload: response.data } )
                  })
                  .catch( err => {
                      console.log(err);
                  });
                //dispatch( { type: 'LISTA_CATEGORIAS', payload: response.data } )
            })
            .catch( err => {
              console.log(err);
            });
    }
}

export function agregarProductosUnidad() {
  return (dispatch, getState) => {
    var state = getState();
    var productos = state.producto.lista;
    var id_unidad = state.unidad.activa.id;
    var datos = [];
    var productosUnidad = [];
    for (let i = 0; i < productos.length; i++) {
      if (productos[i].selected) {
        productosUnidad.push(productos[i]);
        datos.push([id_unidad, productos[i].id])
      }
    }
    axios.post(SERVER + "/api/unidad-producto/", datos)
    .then( result => {
      console.log(result);
      dispatch({type:'AGREGAR_PRODUCTO_UNIDAD', payload: productosUnidad });
    })
    .catch( err => {
      console.log(err);
    });
  }
}

export function agregarOperadoresUnidad() {
  return (dispatch, getState) => {
    var state = getState();
    var operadores = state.operador.lista;
    var id_unidad = state.unidad.activa.id;
    var datos = [];
    var operadoresUnidad = [];
    for (let i = 0; i < operadores.length; i++) {
      if (operadores[i].selected) {
        operadoresUnidad.push(operadores[i]);
        datos.push([id_unidad, operadores[i].id])
      }
    }
    axios.post(SERVER + "/api/unidad-operador/", datos)
    .then( result => {
      console.log(result);
      dispatch({type:'AGREGAR_OPERADOR_UNIDAD', payload: operadoresUnidad });
    })
    .catch( err => {
      console.log(err);
    });
  }
}

export function setImagenBase64(picture) {
  return (dispatch, getState) => {
    dispatch({type: 'SET_IMAGEN_UNIDAD', payload: picture})
  }
}

export function setSelected(index) {
  return (dispatch, getState) => {
    var productos = Object.assign([], getState().producto.lista);
    productos[index].selected = !productos[index].selected;
    dispatch({type: 'SET_PRODUCTOS_SELECTED', payload: productos})
  }
}

export function setSelectedOperadores(index) {
  return (dispatch, getState) => {
    var operador = Object.assign([], getState().operador.lista);
    operador[index].selected = !operador[index].selected;
    dispatch({type: 'SET_OPERADORES_SELECTED', payload: operador})
  }
}
