
export class ServiceLocalStorage {

  public user;

  constructor() {
    this.user = getUser();
  }

  public getUser() {
    var user = localStorage.getItem('user')
    if (user) {
      return JSON.parse(user);
    }
    return null;
  }

  public saveUser(user) {
    localStorage.setItem('user', JSON.stringify(user));
  }
  
}
