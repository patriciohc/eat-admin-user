import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux'
// material ui
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
// components
import TbPedido from './tbPedido';
// actions
import {
  changeUnidadPedido,
  cambiarEstatus,
  asignarRepartidor,
  getListaPedidos,
  getListaRepartidores
} from '../../actions/pedido'

class Pedido extends Component  {

  constructor(props) {
    super(props);
    this.state = {
      repartidor: 0,
    };
  }

  handleChangeRepartidor = (event, index, value) => {
    this.setState({
      repartidor: value,
    });
  };

  renderSelectUnidades() {
    return this.props.unidades.map( (item, index) => {
      return (<MenuItem value={item.id} primaryText={item.nombre} />)
    });
  }

  renderListaProductos () {
    return this.props.activa.productos.map((item, index) => {
      return (<li value={item.id}>{item.nombre}</li>)
    });
  }

  renderListaRepartidores() {
    return this.props.repartidores.map((item, index) => {
      return (<MenuItem value={item.id} primaryText={item.nombre} />)
    })
  }

    renderButton () {
      switch (this.props.activa.estatus) {
        case 1:
          return (<RaisedButton label="Aceptar pedido" primary onClick={()=>this.props.cambiarEstatus(2)}></RaisedButton>)
          break;
        case 2:
          return (
            <div>
              <RaisedButton
                label="Asignar a repartidor"
                onClick={()=>this.props.asignarRepartidor(this.state.repartidor)}
                primary/><br/>
              <SelectField
                floatingLabelText="Seleccione Repartidor"
                value={this.state.repartidor}
                onChange={this.handleChangeRepartidor}>
                {this.renderListaRepartidores()}
            </SelectField>
            </div>
          )
          break;
        case 3:
          return (<RaisedButton label="Cerra Pedido" primary onClick={()=>this.props.cambiarEstatus(4)}></RaisedButton>)
          break;
      }
    }

    getClassEstatus (estatus) {
      switch (estatus) {
        case 1:
          return 'en-espera'
          break;
        case 2:
          return 'en-tienda'
          break
        case 3:
          return 'en_ruta'
          break
        case 4:
          return 'entregado'
          break
      }
    }

    render() {
      return (
        <div>
          <h3>Administracion Pedidos</h3>
            <SelectField
              floatingLabelText="Seleccione Unidad"
              value={this.props.unidadSeleccionada}
              onChange={(event, el, val) => {
                this.props.changeUnidadPedido(event, el, val)
                this.props.getListaPedidos()
              }}>
              {this.renderSelectUnidades()}
            </SelectField>
            <TbPedido pedidos={this.props.pedidos}/>

              {this.props.activa.direccion_entrega ? (
              <div className="row" style={{margin:20}}>
              <div className="detail-pedido col-xs-3">
                <div>
                  <span className="title">Estatus:</span>
                  <span
                    className={this.getClassEstatus(this.props.activa.estatus)}>
                    {this.props.catalogos.estatus[this.props.activa.estatus]
                  }</span>
                </div>
                <div>
                  <span className="title">Id:</span> <span>{this.props.activa.id}</span>
                </div>
                <div>
                  <span className="title">Dirección:</span>
                  <span>{this.props.activa.direccion_entrega.direccion}</span>
                </div>
                <div>
                  <span className="title">Latitud: </span>
                  <span>{this.props.activa.direccion_entrega.lat}</span>
                </div>
                <div>
                  <span className="title">Longitud: </span>
                  <span>{this.props.activa.direccion_entrega.lng}</span>
                </div>
                <div>
                  <span className="title">Fecha:</span>
                  <span>{this.props.activa.fecha_recibido}</span>
                </div>
                <div>
                  <span className="title">Hora:</span>
                  <span>{this.props.activa.hora_recibido}</span>
                </div>
                <div>
                  <span className="title">Productos: </span>
                  <ul>
                    {this.renderListaProductos()}
                  </ul>
                </div>
              </div>
              <div className="col-xs-6">
                {this.renderButton()}
              </div>
            </div>
            ):
                (<h3>Seleccione una unidad</h3>) }
            </div>
        );
    }
}

function mapStateToProps(state) {
  return {
    unidades: state.unidad.lista,
    unidadSeleccionada: state.pedido.unidadSeleccionada,
    activa: state.pedido.activa,
    catalogos: state.catalogos,
    repartidores: state.pedido.repartidores,
    pedidos: state.pedido.lista,
  }
}

export default connect(mapStateToProps, {
  changeUnidadPedido,
  cambiarEstatus,
  asignarRepartidor,
  getListaPedidos
})(Pedido)
