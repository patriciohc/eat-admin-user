import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux'
import {
  getListaProductosUnidad,
  addProductoUnidad,
  getListaProductos
} from '../../actions'
// material-ui
import RaisedButton from 'material-ui/RaisedButton';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn
} from 'material-ui/Table';

import ModalProductos from './modalProductos'

const styleButton = {
  margin: 10,
};

class TbProducto extends Component  {

  constructor(props) {
    super(props);
    this.state = {
      stateModal: 0,
    };
  }

  modalProductos = (value) => {
    this.setState({
      stateModal: value,
    });
  };

  componentWillMount() {
    this.props.getListaProductos();
  }

    renderUsersList() {
        return this.props.productos.map((producto) => {
            return (
                <TableRow key={producto.id}>
                    <TableRowColumn>{producto.id}</TableRowColumn>
                    <TableRowColumn>{producto.nombre}</TableRowColumn>
                    <TableRowColumn>{producto.precio}</TableRowColumn>
                    <TableRowColumn>{producto.imagen}</TableRowColumn>
                    <TableRowColumn>{producto.id_categoria}</TableRowColumn>
                </TableRow>
            )
        })
    }

    render() {
        return (
            <div>
            <div className="row">
                <div className="col-xs-6">
                    <h4>Lista de productos disponibles en la unidad</h4>
                </div>
                <div className="col-xs-6" style={{textAlign: "right"}}>
                    <RaisedButton
                        style={styleButton}
                        label="Agregar Productos"
                        primary
                        onClick={() => this.modalProductos(true)}
                    />
                </div>
            </div>

            <Table>
            <TableHeader
                displaySelectAll={false}
                adjustForCheckbox={false}>
                <TableRow>
                  <TableHeaderColumn>Id</TableHeaderColumn>
                  <TableHeaderColumn>Nombre</TableHeaderColumn>
                  <TableHeaderColumn>precio</TableHeaderColumn>
                  <TableHeaderColumn>imagen</TableHeaderColumn>
                  <TableHeaderColumn>Categoria</TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {this.renderUsersList()}
            </TableBody>
            </Table>
            <ModalProductos
              onClose={() => this.modalProductos(false)}
              open={this.state.stateModal}/>
            </div>
        );
    }
}

function mapStateToProps(state) {
  return {
    modal: state.modalProductos,
    productos: state.unidad.listaProductos
  }
}

export default connect(mapStateToProps, {
  getListaProductosUnidad,
  getListaProductos })(TbProducto)
