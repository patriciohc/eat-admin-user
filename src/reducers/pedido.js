
const initialState =  {
  lista: [],
  unidadSeleccionada: 81,
  activa: {},
  repartidores: []
}

const pedido = (state = initialState, action) => {
  switch (action.type) {
    case 'LISTA_PEDIDOS':
      return {
        ...state,
        activa: {},
        lista: action.payload,
      };
    //return Object.assign({}, state, {unidades: action.payload});
    case 'SET_PEDIDO':
      return {
        ...state,
        activa: action.payload
      };
    case 'SET_UNIDAD_SELECCIONADA':
      return {
        ...state,
        unidadSeleccionada: action.payload,
      };
    case 'LISTA_REPARTIDORES_PEDIDO':
      return {
        ...state,
        repartidores: action.payload,
      };
    default:
    return state
  }
}

export default pedido;
