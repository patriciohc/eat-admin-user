import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux'
import {
  getListaUnidades,
  editarUnidad,
  eliminarUnidad,
  cargarProductos,
  cargarOperadores
} from '../../actions/unidad'

import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';

class TbUnidad extends Component  {


  renderUsersList() {
    return this.props.unidades.map((unidad, index) => {
            return (
                <TableRow key={unidad.id} fixedHeader={true}>
                    <TableRowColumn>{unidad.id}</TableRowColumn>
                    <TableRowColumn>{unidad.nombre}</TableRowColumn>
                    <TableRowColumn>{unidad.direccion}</TableRowColumn>
                    <TableRowColumn>{unidad.telefono}</TableRowColumn>
                    <TableRowColumn>{unidad.hora_apetura}</TableRowColumn>
                    <TableRowColumn>{unidad.hora_cierre}</TableRowColumn>
                    <TableRowColumn>
                        <FlatButton
                            label="Editar"
                            primary
                            onClick={() => {
                              this.props.editarUnidad(index)
                              this.props.cargarProductos(index)
                              this.props.cargarOperadores(index)
                            }}
                        />
                    </TableRowColumn>
                    <TableRowColumn>
                        <FlatButton
                            label="Eliminar"
                            secondary
                            onClick={() => this.props.eliminarUnidad(index)}
                        />
                    </TableRowColumn>
                </TableRow>
            )
    })
  }

    render() {
        return (
            <Table>
            <TableHeader
                displaySelectAll={false}
                adjustForCheckbox={false}
            >
                <TableRow>
                    <TableHeaderColumn>Id</TableHeaderColumn>
                    <TableHeaderColumn>Nombre</TableHeaderColumn>
                    <TableHeaderColumn>Direccion</TableHeaderColumn>
                    <TableHeaderColumn>Télefono</TableHeaderColumn>
                    <TableHeaderColumn>Hora de Apertura</TableHeaderColumn>
                    <TableHeaderColumn>Hora de Cierre</TableHeaderColumn>
                    <TableHeaderColumn></TableHeaderColumn>
                    <TableHeaderColumn></TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {this.renderUsersList()}
            </TableBody>
            </Table>
        );
    }
}

function mapStateToProps(state) {
  return {
    unidades: state.unidad.lista,
    usuario: state.usuario
  }
}

export default connect(mapStateToProps, {
  getListaUnidades,
  editarUnidad,
  eliminarUnidad,
  cargarProductos,
  cargarOperadores
})(TbUnidad)
