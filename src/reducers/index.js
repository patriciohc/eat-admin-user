import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import unidad from './unidad/unidad';
import cliente from './cliente';
import categoria from './categoria';
import producto from './producto';
import map from './unidad/map';
import operador from './operador'
//import modals from './unidad/modals';
import pedido from './pedido';
import catalogos from './catalogos';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
export default createStoreWithMiddleware(
    combineReducers({
        unidad,
        cliente,
        categoria,
        producto,
        map,
        //modals,
        operador,
        pedido,
        catalogos
    })
);
