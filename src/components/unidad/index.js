import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux'
// actions
import {
  crearUnidadad,
  setUnidad,
  initUnidad,
  setPosicion
} from '../../actions/unidad';

import Divider from 'material-ui/Divider';
/* global google */
import _ from "lodash";
//material-ui
import RaisedButton from 'material-ui/RaisedButton';
import {Tabs, Tab} from 'material-ui/Tabs';
// From https://github.com/oliviertassinari/react-swipeable-views
import SwipeableViews from 'react-swipeable-views';
// my components
import FormUnidad from './formUnidad';
import TbUnidad from './tbUnidad';
import Map from './map';
import TbProducto from './tbProducto';
import TbOperador from './tbOperador';

import Autocomplete from 'react-google-autocomplete';

const styleButton = {
  margin: 10,
};

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
  slide: {
    padding: 10,
  },
};

class Unidad extends Component  {

  constructor(props) {
    super(props);
    this.state = {
      slideIndex: 0,
    };
  }

  handleChange = (value) => {
    this.setState({
      slideIndex: value,
    });
  };

  // renderTablaProductos() {
  //   if (this.props.activa.id) {
  //     return (
  //
  //     );
  //   }
  // }

  renderForm() {
    if (this.props.activa && typeof(this.props.activa.id) != "undefined") {
      return (
        <div>
          <div className="row" style={{marginTop:20}}>
            <div className="col-md-5">
              <FormUnidad/>
            </div>
              <div className="col-md-7">
                <Autocomplete
                  style={{width: '90%', marginTop:15, marginBottom: 15}}
                  onPlaceSelected={(place) => {this.props.setPosicion(place)}}
                  types={['address']}
                  componentRestrictions={{country: "mx"}}
                />
                <Map
                  containerElement={<div style={{ height: `400px` }} />}
                  mapElement={<div style={{ height: `100%` }} />}
                  onMapLoad={_.noop}
                  onMapClick={_.noop}
                  markers={this.props.map.marker}
                  onMarkerRightClick={_.noop}
                  activa={this.props.activa}
                  zoom={this.props.map.zoom}
                  center={this.props.map.center}
                />
              </div>
            </div>
            <Tabs
              onChange={this.handleChange}
              value={this.state.slideIndex}>
                <Tab label="Productos" value={0} />
                <Tab label="Repartidores" value={1} />
            </Tabs>
            <SwipeableViews
              index={this.state.slideIndex}
              onChangeIndex={this.handleChange}>
              <div style={{width:'95%'}}>
                <TbProducto/>
              </div>
              <div style={{width:'95%'}}>
                <TbOperador/>
              </div>
            </SwipeableViews>


                <div style={{textAlign: "right"}}>
                { this.props.activa.id ?
                    <RaisedButton
                        style={styleButton}
                        label="Actualizar"
                        primary
                    /> :
                    <RaisedButton
                        style={styleButton}
                        label="Guardar"
                        onClick={() => this.props.crearUnidadad()} primary
                    /> }
                </div>
            </div>
            )
        }
    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-md-7">
                        <h3>Administracion de Unidades</h3>
                    </div>
                    <div className="col-md-5" style={{textAlign: "right"}}>
                        <RaisedButton
                            style={styleButton}
                            label="Registrar Unidad"
                            onClick={() => this.props.initUnidad()} primary
                        />
                    </div>
                </div>
                <TbUnidad/>
                {this.renderForm()}
            </div>
        );
    }
}

function mapStateToProps(state) {
  return {
    unidades: state.unidad.lista,
    activa: state.unidad.activa,
    map: state.map
  }
}

export default connect(mapStateToProps,
    { crearUnidadad,
      setUnidad,
      initUnidad,
      setPosicion
  })(Unidad)
