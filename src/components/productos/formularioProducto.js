import React from "react";
import TextField from "material-ui/TextField";
import RaisedButton from 'material-ui/RaisedButton';
import { connect } from 'react-redux';
import UploadPreview from 'material-ui-upload/UploadPreview';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import { validator } from '../../helpers/validators'

// actions
import {
  crearProducto,
  setProducto,
  changeCategoriaProducto,
  setImagenBase64,
  GuardarEdicionProducto
} from '../../actions/producto';

class FormularioProducto extends React.Component {
  errors = {}
  onChange = (event) => {
    this.props.setProducto(event)
    validator(event, this.errors)
  }

    renderSelectCategorias() {
        return this.props.categorias.map( (item, index) => {
            return (
                <MenuItem value={item.id} primaryText={item.nombre} />
            )
        });
    }

    render () {
      return (
        <div className="row" style={{margin:20, marginBottom:50}}>
          <form>
            <div className="col-md-6">
              <TextField
                name="nombre"
                floatingLabelText="Nombre*"
                floatingLabelFixed={true}
                fullWidth={true}
                required
                value={this.props.producto.nombre}
                onChange={this.onChange}
                errorText={this.errors['nombre']}
              />
              <TextField
                name="descripcion"
                floatingLabelText="Descripcion*"
                fullWidth={true}
                floatingLabelFixed={true}
                value={this.props.producto.descripcion}
                onChange={this.onChange}
                required
                errorText={this.errors['descripcion']}
              />
              <TextField
                name="precio_publico"
                floatingLabelText="Precio*"
                fullWidth={true}
                floatingLabelFixed={true}
                value={this.props.producto.precio_publico}
                onChange={this.onChange}
                type="number"
                errorText={this.errors['precio_publico']}
              />
              <TextField
                name="codigo_barras"
                floatingLabelText="Codigo de Barras (opcional)"
                fullWidth={true}
                floatingLabelFixed={true}
                value={this.props.producto.codigo_barras}
                onChange={this.props.setProducto}
              />
              <SelectField
                  floatingLabelText="Seleccione una Categoria"
                  value={this.props.producto.id_categoria}
                  onChange={this.props.changeCategoriaProducto}>
                      {this.renderSelectCategorias()}
              </SelectField>
            </div>
            <div className="col-md-6" style={{paddingTop:40}}>
              <UploadPreview
                title="Imagen"
                label="Seleccionar imagen"
                initialItems={this.props.producto.imagenBase64}
                onChange={this.props.setImagenBase64}/>
              <div className="col-md-12" style={{paddingTop:30}}>
                { this.props.producto.id == null ?
                  (<RaisedButton className="pull-right" label="Guardar Producto" onClick={this.props.crearProducto} primary />) :
                  (<RaisedButton className="pull-right" label="Guardar Edición" onClick={this.props.GuardarEdicionProducto} primary />)
                }
              </div>
            </div>
      </form>
    </div>
    );
  }
}

function mapStateToProps(state) {
  return {

  }
}

export default connect(mapStateToProps, {
  crearProducto,
  setProducto,
  setImagenBase64,
  GuardarEdicionProducto,
  changeCategoriaProducto })(FormularioProducto)
