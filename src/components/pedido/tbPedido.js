import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux'
import {
  getListaPedidos,
  setPedido,
  getListaRepartidores
} from '../../actions/pedido';

import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';

class TbPedido extends Component  {

  componentWillMount() {
    this.props.getListaPedidos();
  }

    renderPedidoList() {
        return this.props.pedidos.map((pedido, index) => {
            return (
                <TableRow key={pedido.id} >
                    <TableRowColumn>{pedido.id}</TableRowColumn>
                    <TableRowColumn >{this.props.catalogos.estatus[pedido.estatus]}</TableRowColumn>
                    <TableRowColumn>{pedido.fecha_recibido}</TableRowColumn>
                    <TableRowColumn>{pedido.hora_recibido}</TableRowColumn>
                    <TableRowColumn>{pedido.direccion_entrega.direccion}</TableRowColumn>
                    <TableRowColumn>
                        <FlatButton
                            label="Ver pedido"
                            primary
                            onClick={() => {
                              this.props.setPedido(index)
                              this.props.getListaRepartidores()
                            }}
                        />
                    </TableRowColumn>
                </TableRow>
            )
        })
    }

    render() {
        return (
            <Table>
            <TableHeader
                displaySelectAll={false}
                adjustForCheckbox={false}>
                <TableRow>
                    <TableHeaderColumn>id</TableHeaderColumn>
                    <TableHeaderColumn>estatus</TableHeaderColumn>
                    <TableHeaderColumn>Fecha</TableHeaderColumn>
                    <TableHeaderColumn>Hora</TableHeaderColumn>
                    <TableHeaderColumn>Direccion</TableHeaderColumn>
                    <TableHeaderColumn>Ver</TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {this.renderPedidoList()}
            </TableBody>
            </Table>
        );
    }
}

function mapStateToProps(state) {
  return {
    catalogos: state.catalogos
  }
}

export default connect(mapStateToProps, {
  getListaPedidos,
  setPedido,
  getListaRepartidores
})(TbPedido)
