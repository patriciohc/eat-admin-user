import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux'
import {
  getListaCategorias,
  editarCategoria,
  eliminarCategoria,
} from '../../actions/categoria'

import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';

class TbCategoria extends Component  {

  componentWillMount() {
    this.props.getListaCategorias();
  }

    renderUsersList() {
        return this.props.categorias.map((categoria, index) => {
            return (
                <TableRow key={categoria.id}>
                    <TableRowColumn>{categoria.id}</TableRowColumn>
                    <TableRowColumn>{categoria.nombre}</TableRowColumn>
                    <TableRowColumn>{categoria.descripcion}</TableRowColumn>
                    <TableRowColumn>{categoria.imagen}</TableRowColumn>
                    <TableRowColumn>
                        <FlatButton
                            label="Editar"
                            primary
                            onClick={() => this.props.editarCategoria(index)}
                        />
                    </TableRowColumn>
                    <TableRowColumn>
                        <FlatButton
                            label="Eliminar"
                            secondary
                            onClick={() => this.props.eliminarCategoria(index)}
                        />
                    </TableRowColumn>
                </TableRow>
            )
        })
    }

    render() {
        return (
            <Table>
            <TableHeader
                displaySelectAll={false}
                adjustForCheckbox={false}
            >
                <TableRow>
                    <TableHeaderColumn>Id</TableHeaderColumn>
                    <TableHeaderColumn>Nombre</TableHeaderColumn>
                    <TableHeaderColumn>Descripcion</TableHeaderColumn>
                    <TableHeaderColumn>Imagen</TableHeaderColumn>
                    <TableHeaderColumn></TableHeaderColumn>
                    <TableHeaderColumn></TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {this.renderUsersList()}
            </TableBody>
            </Table>
        );
    }
}

function mapStateToProps(state) {
  return {
    categorias: state.categoria.lista
  }
}

export default connect(mapStateToProps, {
  getListaCategorias,
  editarCategoria,
  eliminarCategoria,
})(TbCategoria)
