import { createStore, applyMiddleware, combineReducers } from 'redux';

const initialState = {
  marker: [],
  zoom: 5,
  center: { lat: 21.0, lng: -100.0 }
}

const map = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_POSICION':
          return {
            marker: [action.payload],
            zoom: 13,
            center: action.payload,
          };
        default:
            return state
    }
}

export default map;
