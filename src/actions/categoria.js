import axios from 'axios'
import { guardar as guardarImagen, getExtencion } from './imagen';
import swal from 'sweetalert2'
import { validatorForm } from '../helpers/validators'
import { SERVER } from '../settings';

// categorias
export function getListaCategorias() {
  console.log("execute --> getListaCategorias");
    return (dispatch, getState) => {
      var idCliente = getState().cliente.id;
        axios.get(SERVER + '/api/categoria-cliente?id_cliente='+idCliente)
            .then((response) => {
                console.log(response.data);
                dispatch( { type: 'LISTA_CATEGORIAS', payload: response.data } )
            })
            .catch( err => {
              console.log(err);
            });
    }
}

export function setImagenBase64(picture) {
  return (dispatch, getState) => {
    dispatch({type: 'SET_IMAGEN_CATEGORIA', payload: picture})
  }
}

const requires = ['nombre','descripcion']
export function crearCategoria() {
  console.log("execute --> crearUnidadad");
  //console.log(categoria);
  return (dispatch, getState) => {
    var state = getState();
    var categoria = Object.assign({}, state.categoria.activa);
    if (!validatorForm(requires, categoria)) {
      swal("Los campos marcados con * son requeridos")
      return
    }
    categoria.id_cliente = state.cliente.id;
    var imagenBase64;
    for (let value in state.categoria.imagenBase64) {
      imagenBase64 = state.categoria.imagenBase64[value];
      categoria.imagen = categoria.id_cliente + "-cat-" + (new Date()).getTime() + "." + getExtencion(imagenBase64)
      break
    }
    delete categoria.id
    axios.post(SERVER + '/api/categoria/', categoria)
    .then((response) => {
      console.log(response.data);
      categoria.id = response.data.id;
      return guardarImagen(imagenBase64, categoria.imagen)
    })
    .then(result => {
      console.log(result);
      dispatch( { type: 'CREAR_CATEGORIA', payload: categoria } )
    })
    .catch(err => {
      console.log(err);
    })
  }
}

export function setCategoria(event) {
    console.log("eject --> setUnidadad");
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    return (dispatch, getState) => {
      var categoria = Object.assign({}, getState().categoria.activa);
      categoria[name] = value;
      console.log(categoria);
      dispatch( { type: 'SET_CATEGORIA', payload: categoria } )
    }
}

export function editarCategoria(index) {
  console.log("execute --> editarCategoria");
  return (dispatch, getState) => {
    var obj = getState().categoria.lista[index]
    dispatch( { type: 'SET_CATEGORIA', payload: obj } )
  }
}

export function eliminarCategoria(index) {
  console.log("execute --> eliminarCategoria");
  return (dispatch, getState) => {
    let objs = getState().categoria.lista;
    let id = objs[index].id;
    axios.delete(SERVER + '/api/categoria?id='+id)
    .then(response => {
      let id_cliente = getState().cliente.id
      axios.get(SERVER + '/api/categoria-cliente?id_cliente='+id_cliente)
      .then( response => {
        console.log(response.data);
        dispatch( { type: 'LISTA_CATEGORIAS', payload: response.data } )
      })
      .catch( err => {
        console.log(err);
      });
    })
    .catch( err => {
      console.log(err);
    });
  }
}

// export function changeUnidad(event, index, value) {
//     return (dispatch, getState) => {
//         axios.get('https://app-base-ws.herokuapp.com/api/categoria?id_unidad='+value)
//         .then((response) => {
//             console.log(response.data);
//             dispatch({
//                 type: 'CHANGE_UNIDAD',
//                 payload: {
//                     lista: response.data,
//                     unidadSeleccionada: value
//                 }
//             })
//         })
//     }
// }
