// Dependencies
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

// Components
import App from './components/App';
import Login from './components/login';
import Page404 from './components/Page404';
import MenuBar from './components/menuBar'
import Unidad from './components/unidad';
import Categoria from './components/categoria';
import Producto from './components/productos';
import Operador from './components/registro-operador';
import Pedido from './components/pedido';
//actions
import {
    getListaUnidades,
    getListaCategorias,
} from './actions';
// redux
import { connect } from 'react-redux';

const AppRoutes = (props) => {

  function goToLogin() {
     props.history.push('/admin-client/');
  }

  function goToSession() {
    props.getListaUnidades();
    props.getListaCategorias();
  }

    return (<App>
        <Switch>
            <Route exact path="/admin-client/unidad" render={() => <MenuBar goToLogin={goToLogin} goToSession={goToSession}><Unidad /></MenuBar>} />
            <Route exact path="/admin-client/categoria" render={() => <MenuBar goToLogin={goToLogin} goToSession={goToSession}><Categoria /></MenuBar>} />
            <Route exact path="/admin-client/producto" render={() => <MenuBar goToLogin={goToLogin} goToSession={goToSession}><Producto /></MenuBar>} />
            <Route exact path="/admin-client/operador" render={() => <MenuBar goToLogin={goToLogin} goToSession={goToSession}><Operador /></MenuBar>} />
            <Route exact path="/admin-client/pedido" render={() => <MenuBar goToLogin={goToLogin} goToSession={goToSession}><Pedido /></MenuBar>} />
            <Route exact path="/admin-client" component={Login} />
            <Route exact path="/admin-client/super-admin" component={Login} />
            <Route component={Page404} />
        </Switch>
    </App>);
}

function mapStateToProps(state) {
  return {}
}
//export default connect(mapStateToProps, { getListaUnidades })(AppRoutes);
//export default AppRoutes

export default withRouter(
    connect(mapStateToProps, { getListaUnidades, getListaCategorias })(AppRoutes)
);
