import React from "react";
import { connect } from 'react-redux';
import { withGoogleMap, GoogleMap, Marker } from "react-google-maps";
// actions
//import { setUnidad } from '../../actions';

const styleButton = {
  marginTop: 12,
};

export default Map = withGoogleMap(props => {
  return (
    <div className="row">
      <div className="col-md-12">
        <GoogleMap
          zoom={props.zoom}
          center={props.center}
        >
        {props.markers.map((marker, index) => (
          <Marker
            draggable={true}
            onDragEnd={(event)=> {
              console.log(event);
            }}
            position={{ lat: marker.lat, lng: marker.lng }}
          />
        ))}
        </GoogleMap>
      </div>
    </div>
  );
});
