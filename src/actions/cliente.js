import axios from 'axios'
import { SERVER } from '../settings';

// cliente
export function loginCliente(correo_electronico, password, callback) {
    return (dispatch, getState) => {
        axios.post(SERVER + '/api/login-cliente', {correo_electronico, password})
            .then((response) => {
                console.log(response.data);
                dispatch( { type: 'LOGIN', payload: response.data } )
                callback();
            })
            .catch(err => {
                console.log(err);
            })
    }
}

export function initUsuario(callback, callback2) {
    var usuario = localStorage.getItem("usuario");
    if (usuario) {
        usuario = JSON.parse(usuario);
    } else {
        usuario = {};
    }
    return (dispatch, getState) => {
        dispatch( { type: 'INIT_USUARIO', payload: usuario } )
        if (usuario.id) {
          if (callback) callback();
        } else {
          if (callback2) callback2();
        }
    }
}
