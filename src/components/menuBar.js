import React from 'react';
// material-ui
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import { List, ListItem } from 'material-ui/List';
import { Link, browserHistory } from 'react-router-dom';
import { connect } from 'react-redux';

import { loginCliente, initUsuario } from '../actions';
import { getCatalogos } from '../actions/catalogos';

class MenuBar extends React.Component  {
    // constructor(props) {
    //     super(props);
    //     //if (this.props.idCliente)
    //
    // }

    componentWillMount() {
      this.state = {
          open: false
      };
      this.handleToggle = this.handleToggle.bind(this);
      //this.handleTitleTouchTap = this.handleTitleTouchTap.bind(this);
      this.props.initUsuario(this.props.goToSession, this.props.goToLogin);
      this.props.getCatalogos();
    }

    handleToggle() {
        this.setState({ open: !this.state.open });
    }

    render() {
    return (
        <div>
            <AppBar
                title="Administracion"
                iconClassNameRight="muidocs-icon-navigation-expand-more"
                onLeftIconButtonTouchTap={this.handleToggle}
            />
            <Drawer
                docked={false}
                width={200}
                open={this.state.open}
                onRequestChange={(open) => this.setState({ open })}
            >
                <List>
                    <ListItem
                        containerElement={<Link to="/admin-client/pedido" />}
                        onTouchTap={this.handleToggle}
                        primaryText="Pedidos"
                    />
                    <ListItem
                        containerElement={<Link to="/admin-client/unidad" />}
                        onTouchTap={this.handleToggle}
                        primaryText="Registro de Unidades"
                    />
                    <ListItem
                        containerElement={<Link to="/admin-client/categoria" />}
                        onTouchTap={this.handleToggle}
                        primaryText="Registro de Categorias"
                    />
                    <ListItem
                        containerElement={<Link to="/admin-client/producto" />}
                        onTouchTap={this.handleToggle}
                        primaryText="Registro de Productos"
                    />
                    <ListItem
                        containerElement={<Link to="/admin-client/operador" />}
                        onTouchTap={this.handleToggle}
                        primaryText="Registro de Operadores"
                    />
                </List>
            </Drawer>
            <div className="container">
                {this.props.children}
            </div>
        </div>
    );
  }
}

function mapStateToProps () {
  return {}
}

export default connect(mapStateToProps, { initUsuario, getCatalogos })(MenuBar)
