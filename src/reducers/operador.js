
const initialState =  {
        activa: {},
        lista: [],
        imagenBase64: {},
    }

const operador = (state = initialState, action) => {
    switch (action.type) {
        case 'LISTA_OPERADOR':
            return {
                activa: {
                    nombre: "",
                    apellido_paterno: "",
                    apellido_materno: "",
                    correo_electronico: "",
                },
                lista: action.payload
            };
            case 'CREAR_OPERADOR':
                return {
                    activa: {
                      nombre: "",
                      apellido_paterno: "",
                      apellido_materno: "",
                      correo_electronico: "",
                    },
                    imagenBase64: {},
                    lista: state.lista.concat(action.payload)
                };
        //return Object.assign({}, state, {unidades: action.payload});
        case 'SET_OPERADOR':
            return {
                activa: action.payload,
                lista: state.lista
            };
        case 'SET_IMAGEN_OPERADOR':
            return {
              ...state,
              imagenBase64: action.payload
            };
        case 'SET_OPERADORES_SELECTED':
            return {
              ...state,
              lista: action.payload
            }
        default:
            return state
    }
}

export default operador;
