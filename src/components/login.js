import React from "react";
import TextField from "material-ui/TextField";
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import AppBar from 'material-ui/AppBar';

import { connect } from 'react-redux';

import { loginCliente, initUsuario } from '../actions';

const style = {
  height: 300,
  width: 400,
  margin: 20,
  textAlign: 'center',
  display: 'inline-block',
};


class Login extends React.Component {

    componentWillMount() {
        this.props.initUsuario(this.goToSession.bind(this));
    }

    goToSession() {
        this.props.history.push('/admin-client/unidad');
    }

    clickOnLogin() {
        var correo_electronico = document.getElementById("correo_electronico").value;
        var password = document.getElementById("password").value;
        this.props.loginCliente(correo_electronico, password, this.goToSession.bind(this));
    }

  render() {
    return (
    <div className="row center" style={{paddingTop: 60}}>
        <Paper style={style} zDepth={2} >
        <AppBar
            title="Bienvenido"
            showMenuIconButton={false}
        />
        <form className="col-md-12" >
            <TextField
                fullWidth={true}
                name="correo_electronico"
                id="correo_electronico"
                floatingLabelText="Nombre de usuario"
            />
            <TextField
                fullWidth={true}
                name="password"
                id="password"
                floatingLabelText="Constraseña"
            />
            <RaisedButton className="pull-right" onClick={() => this.clickOnLogin()} label="Entrar" primary />
      </form>
      </Paper>
    </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    usuario: state.usuario
  }
}

export default connect(mapStateToProps, { loginCliente, initUsuario })(Login)
