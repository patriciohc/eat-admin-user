import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux'
import { getListaOperadores } from '../../actions/operador'
// material-ui
import RaisedButton from 'material-ui/RaisedButton';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn
} from 'material-ui/Table';

import ModalOperadores from './modalOperadores'

const styleButton = {
  margin: 10,
};

class TbOperador extends Component  {

  constructor(props) {
    super(props);
    this.state = {
      stateModal: 0,
    };
  }

  setEstatusmodal = (value) => {
    this.setState({
      stateModal: value,
    });
  };

  componentWillMount() {
    //this.props.getListaUnidades();
    this.props.getListaOperadores();
  }

    renderUsersList() {
        return this.props.operadores.map((obj) => {
            return (
                <TableRow key={obj.id}>
                  <TableHeaderColumn>Id</TableHeaderColumn>
                  <TableRowColumn>{obj.id}</TableRowColumn>
                  <TableRowColumn>{obj.nombre}</TableRowColumn>
                  <TableRowColumn>{obj.apellido_paterno}</TableRowColumn>
                  <TableRowColumn>{obj.apellido_materno}</TableRowColumn>
                  <TableRowColumn>{obj.correo_electronico}</TableRowColumn>
                  <TableRowColumn>{obj.rol}</TableRowColumn>
                </TableRow>
            )
        })
    }

    render() {
        return (
            <div>
            <div className="row">
                <div className="col-xs-6">
                    <h4>Lista de productos disponibles en la unidad</h4>
                </div>
                <div className="col-xs-6" style={{textAlign: "right"}}>
                    <RaisedButton
                        style={styleButton}
                        label="Agregar Repatidor"
                        primary
                        onClick={() => this.setEstatusmodal(true)}
                    />
                </div>
            </div>

            <Table>
            <TableHeader
                displaySelectAll={false}
                adjustForCheckbox={false}>
                <TableRow>
                    <TableHeaderColumn>Nombre</TableHeaderColumn>
                    <TableHeaderColumn>Ap Paterno</TableHeaderColumn>
                    <TableHeaderColumn>Ap Materno</TableHeaderColumn>
                    <TableHeaderColumn>Email</TableHeaderColumn>
                    <TableHeaderColumn>Rol</TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {this.renderUsersList()}
            </TableBody>
            </Table>
            <ModalOperadores
            onClose={() => this.setEstatusmodal(false)}
            open={this.state.stateModal}/>
          </div>
        );
    }
}

function mapStateToProps(state) {
  return {
    modal: state.modalProductos,
    operadores: state.unidad.listaOperadores
  }
}

export default connect(mapStateToProps, {
  //getListaOperadoresUnidad,
  getListaOperadores })(TbOperador)
