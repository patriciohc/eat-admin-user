import axios from 'axios'
import { SERVER } from '../settings'


export function getExtencion(base64) {
  let extencion = base64.split(',')[0];
  let parts = extencion.split(/[:/;]+/)
  if (parts[1] =! 'image') return null
  extencion = parts[2].split('.')
  extencion = extencion[extencion.length - 1]
  return extencion;
}
// categorias
export function guardar(base64, nombre) {

  console.log("execute --> guardarImagen");
  var settings = {
    "async": true,
    "url": "http://www.shell-system.com/ws.php",
    "method": "POST",
    "headers": {
      "content-type": "text/xml"
    },
    "data":
    `<Envelope xmlns="http://schemas.xmlsoap.org">
      <Body>
        <guardarImagen>
          <base64Img>${base64}</base64Img>
          <nameImg>${nombre}</nameImg>
        </guardarImagen>
      </Body>
    </Envelope>`
  }
  return new Promise((resolve, reject) => {
    axios(settings)
    .then(result => {
      console.log(result)
      resolve(result)
    })
    .catch(err => reject(err))
  })
}
